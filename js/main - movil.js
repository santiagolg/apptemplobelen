/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

var menu,botones, secciones, personas, barraMenu, correoRegistro,nombreRegistro, contrasenaRegistro,telefonoRegistro,direccionRegistro, objetoPersona,
correoIngreso,contrasenaIngreso, usuarioIngresado, listaCursos, cursosRegistro, palabra, seccion, cursosPersona, referencia, formularioInicio,formularioRegistro,
personasJson, encontroPersona, menuMovil, estadoMenu;

var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        inicializacionVariables();
        inicializacionEventos();
    }

};

//funcion que inicializa todas las variables necesarias
function inicializacionVariables(){
    botones = [];
    personas = [];
    listaCursos = [];
    cursosRegistro = [];
    cursosVistos = [];
    //array de los botones usados para la navegacion
    botones[0] = document.getElementById("seccion_inicio");
    botones[1] = document.getElementById("seccion_ingreso");
    botones[2] = document.getElementById("seccion_calendario");
    botones[3] = document.getElementById("seccion_cursos");
    botones[4] = document.getElementById("seccion_registro");
    botones[5] = document.getElementById("volver_ingreso");
    botones[6] = document.getElementById("ir_cursos");
    botones[7] = document.getElementById("ir_calendario");
    botones[8] = document.getElementById("salir_inicio");
    //Array de las personas en el local storage
    personasJson = {};
    //True si la persona ya existe
    encontroPersona = false;
    //Estado del menu
    estadoMenu = false;
    //botones del menu
    menu = document.getElementById("menu");
    menuMovil = document.getElementById("menu_desplegable");
    barraMenu = document.getElementById("barra_menu");
    //Campos del registro
    formularioRegistro = document.getElementById("form_registro");
    correoRegistro = document.getElementById("correo");
    nombreRegistro = document.getElementById("nombre");
    contrasenaRegistro = document.getElementById("contrasena");
    telefonoRegistro = document.getElementById("telefono");
    direccionRegistro = document.getElementById("direccion");
    objetoPersona = {};
    cursosPersona = [];
    //Campos ingreso
    formularioInicio = document.getElementById("form_ingreso");
    correoIngreso = document.getElementById("correoIngreso");
    contrasenaIngreso = document.getElementById("contrasenaIngreso");
    usuarioIngresado = {};
    //Array de los listaCursos seleccionados en el registro
    cursosRegistro[0] = document.getElementById("curso_creemos");
    cursosRegistro[1] = document.getElementById("curso_lideres");
    cursosRegistro[2] = document.getElementById("curso_padres");
    //Array de los listaCursos
    listaCursos[0] = document.getElementById("creemos");
    listaCursos[1] = document.getElementById("lideres");
    listaCursos[2] = document.getElementById("padres");
    //variables para la funcion procesar click
    palabra = "";
    seccion = "";
    //secciones de la pagina web
    secciones = [];
    secciones[0] = document.getElementById("inicio");
    secciones[1] = document.getElementById("ingreso");
    secciones[2] = document.getElementById("calendario");
    secciones[3] = document.getElementById("cursos");
    secciones[4] = document.getElementById("registro");      
}

//funcion que inicializa los eventos de los botones
function inicializacionEventos(){
    formularioRegistro.addEventListener("submit",ingresarRegistro);
    formularioInicio.addEventListener("submit",ingresarUsuario);
    menuMovil.addEventListener("click",desplegarMenu);
    for(var i in botones)
    {
        botones[i].addEventListener("click",procesarClick);
    }

}

//Salida del usuario
function salirUsuario(evt){
    cambiarMenu();
    reiniciarCursos();
    usuarioIngresado = {};
    alert("Salida con éxito");
}

function reiniciarCursos(){
    for (var i in listaCursos) {
        listaCursos[i].className = "list-group-item";
    }
}

//funcion que valida que los campos de registro no esten vacios e ingresa la persona en el localstorage
function ingresarRegistro(evt){
    encontroPersona = false;
    encontrarPersona();
    objetoPersona = {};
    if(encontroPersona == false){
        objetoPersona.nombre = nombreRegistro.value;
        objetoPersona.correo = correoRegistro.value;
        objetoPersona.contrasena = contrasenaRegistro.value;
        objetoPersona.direccion = direccionRegistro.value;
        objetoPersona.telefono = telefonoRegistro.value;
        registrarCursosRegistro();
        objetoPersona.cursos = cursosPersona;
        personas.push(objetoPersona);
        localStorage.setItem("Personas",JSON.stringify(personas));
        formularioRegistro.reset();
        alert("¡Usuario registrado!");
        ocultar();
        secciones[1].className = "animated fadeIn";
    }   
}

//funcion que mira que listaCursos eligio el usuario en el registro
function registrarCursosRegistro(evt){
        cursosPersona = [];
        for (var i in cursosRegistro) {
            if(cursosRegistro[i].checked == true){
                cursosPersona[i] = cursosRegistro[i].value;
            }
        }
}
//funcion busca a una persona en el local storage
function encontrarPersona(evt){
    personasJson = JSON.parse(localStorage.getItem("Personas"));
    for (var i in personasJson) {
        if(personasJson[i].correo == correoRegistro.value){
            alert("El correo ya existe");
            encontroPersona = true;
        }
    }
}

//funcion que valida que los campos de ingreso no esten vacios y que la persona se encuentre en el localstorage
function ingresarUsuario(evt){
    personasJson = JSON.parse(localStorage.getItem("Personas"));
    encontroPersona = false;
    for (var i in personasJson) {
        if(personasJson[i].correo == correoIngreso.value && personasJson[i].contrasena == contrasenaIngreso.value ){
            alert("¡Usuario encontrado!\n"+personasJson[i].nombre);
            usuarioIngresado=personasJson[i];
            llenarCursos();
            cambiarMenu();
            encontroPersona = true;
            formularioInicio.reset();
            ocultar();
            secciones[0].className = "animated fadeIn";
            botones[0].className = "ocultar";
            barraMenu.className = "navbar navbar-inverse navbar-fixed-top absoluto fondo-menu";
        }
    }
    if (encontroPersona == false) {
        alert("El usuario y la contraseña no existe o no son validos")
    }

}

//cambia el menu cuando el usuario ingresa o cuando sale
function cambiarMenu(evt){
    if(botones[8].className == "btn btn-link ocultar"){
        botones[1].className = "btn btn-link ocultar";
        botones[8].className = "btn btn-link";
    }else{
        botones[1].className = "btn btn-link";
        botones[8].className = "btn btn-link ocultar";
    }

}


//Despliga u oculta el menu en los dispositivos moviles
function desplegarMenu(evt) {
    if(estadoMenu == false && this.id == "menu_desplegable"){
        menu.className = "mostrar navbar-toggle collapsed animated fadeInDown";
        
        estadoMenu = true;
    }
    else if (estadoMenu == true){
    menu.className = "ocultar navbar-toggle collapsed animated fadeOut";
        estadoMenu = false;
    }   
}

//Llena los cursos que el usuario ingresado ya vio
function llenarCursos(evt){
    for (var i in usuarioIngresado.cursos) {
        if(usuarioIngresado.cursos[i] != null){
            listaCursos[i].className = "list-group-item list-group-item-success";
        }
    }
}

//Funcion que permite la navegacion entre las diferentes secciones
function procesarClick(evt)
{
    palabra = this.id;
    seccion = palabra.split("_")[1];
    if (palabra == "salir_inicio") {
        salirUsuario();
    }
    if(seccion == "inicio"){
        botones[0].className = "ocultar";
        barraMenu.className = "navbar navbar-inverse navbar-fixed-top absoluto fondo-menu";
    }else{
        botones[0].className = "btn btn-link";
        barraMenu.className = "navbar navbar-inverse navbar-fixed-top relativo fondo-menu";
    }
    referencia = document.getElementById(seccion);
    ocultar();
    referencia.className = "animated fadeIn";
    desplegarMenu();

}

//oculta todas las secciones
function ocultar()
{
    for(var i in secciones)
    {
        secciones[i].className = "ocultar";
    }
}
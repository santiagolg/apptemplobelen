//Llama la funcion init cuando carga la pagina
window.onload = inicio;
// Creacion de las variables
var menu,botones, secciones, personas, barraMenu, correoRegistro,nombreRegistro, contrasenaRegistro,telefonoRegistro, objetoPersona,
correoIngreso,contrasenaIngreso, usuarioIngresado, listaCursos, cursosRegistro, palabra, seccion, cursosPersona, referencia, formularioInicio,formularioRegistro,
personasJson, encontroPersona, menuMovil, estadoMenu, configuracion, errorCode, errorMessage;
//Primera funcion que se ejecuta
function inicio() {
	inicializacionVariables();
	inicializacionEventos();
}

//funcion que inicializa todas las variables necesarias
function inicializacionVariables(){
	// Initialize Firebase
  	configuracion = {
	    apiKey: "AIzaSyBeJehx5_EGM3oMefS4uj3Ue4riTaFlJzc",
	    authDomain: "templo-belen.firebaseapp.com",
	    databaseURL: "https://templo-belen.firebaseio.com",
	    projectId: "templo-belen",
	    storageBucket: "",
	    messagingSenderId: "625344605835"
  	};
  	//Diferentes array que se usaran
	botones = [];
	personas = [];
	listaCursos = [];
	cursosRegistro = [];
	cursosVistos = [];
	//array de los botones usados para la navegacion
	botones[0] = document.getElementById("seccion_inicio");
	botones[1] = document.getElementById("seccion_ingreso");
	botones[2] = document.getElementById("seccion_calendario");
	botones[3] = document.getElementById("seccion_cursos");
	botones[4] = document.getElementById("seccion_registro");
	botones[5] = document.getElementById("volver_ingreso");
	botones[6] = document.getElementById("ir_cursos");
	botones[7] = document.getElementById("ir_calendario");
	botones[8] = document.getElementById("salir_inicio");
	//Array de las personas en el local storage
	personasJson = {};
	//True si la persona ya existe
	encontroPersona = false;
	//Estado del menu
	estadoMenu = false;
	//botones del menu
	menu = document.getElementById("menu");
	menuMovil = document.getElementById("menu_desplegable");
	barraMenu = document.getElementById("barra_menu");
	//Campos del registro
	formularioRegistro = document.getElementById("form_registro");
	correoRegistro = document.getElementById("correo");
	nombreRegistro = document.getElementById("nombre");
	contrasenaRegistro = document.getElementById("contrasena");
	telefonoRegistro = document.getElementById("telefono");
	objetoPersona = {};
	cursosPersona = [];
	//Campos ingreso
	formularioInicio = document.getElementById("form_ingreso");
	correoIngreso = document.getElementById("correoIngreso");
	contrasenaIngreso = document.getElementById("contrasenaIngreso");
	//Array de los listaCursos seleccionados en el registro
	cursosRegistro[0] = document.getElementById("curso_creemos");
	cursosRegistro[1] = document.getElementById("curso_lideres");
	cursosRegistro[2] = document.getElementById("curso_padres");
	//Array de los listaCursos
	listaCursos[0] = document.getElementById("creemos");
	listaCursos[1] = document.getElementById("lideres");
	listaCursos[2] = document.getElementById("padres");
	//variables para la funcion procesar click
	palabra = "";
	seccion = "";
	//secciones de la pagina web
	secciones = [];
	secciones[0] = document.getElementById("inicio");
	secciones[1] = document.getElementById("ingreso");
	secciones[2] = document.getElementById("calendario");
	secciones[3] = document.getElementById("cursos");
	secciones[4] = document.getElementById("registro");
}

//funcion que inicializa los eventos de los botones
function inicializacionEventos(){
	firebase.initializeApp(configuracion);
	formularioRegistro.addEventListener("submit",ingresarRegistro);
	formularioInicio.addEventListener("submit",ingresarUsuario);
	menuMovil.addEventListener("click",desplegarMenu);
	for(var i in botones)
	{
		botones[i].addEventListener("click",procesarClick);
	}

}

//Salida del usuario
function salirUsuario(evt){
	firebase.auth().signOut().then(function() {
	  // Sign-out successful.
	  usuarioIngresado = null;
	  alert("Salida con éxito");
	  cambiarMenu();
	  reiniciarCursos();
	}).catch(function(error) {
	  // An error happened.
	});	
}

function reiniciarCursos(){
	for (var i in listaCursos) {
		listaCursos[i].className = "list-group-item";
	}
}

//funcion que valida que los campos de registro no esten vacios e ingresa la persona en el localstorage
function ingresarRegistro(evt){
	firebase.auth().createUserWithEmailAndPassword(correoRegistro.value, contrasenaRegistro.value).then(function(){
		usuarioIngresado = firebase.auth().currentUser;
		usuarioIngresado.updateProfile({
			displayName: nombreRegistro.value,
		});
		usuarioIngresado.
		secciones[4].className = "ocultar";
		secciones[0].className = "animated fadeIn";
		botones[0].className = "ocultar";
		barraMenu.className = "navbar navbar-inverse navbar-fixed-top absoluto fondo-menu";
		cambiarMenu();
		alert("¡Usuario Registrado!\n"+nombreRegistro.value);
	}).catch(function(error) {
	  // Handle Errors here.
	  errorCode = error.code;
	  errorMessage = error.message;
	  alert("No se pudo registrar el usuario");
	  //
	});
	

}

//funcion que mira que listaCursos eligio el usuario en el registro
function registrarCursosRegistro(evt){
		cursosPersona = [];
		for (var i in cursosRegistro) {
			if(cursosRegistro[i].checked == true){
				cursosPersona[i] = cursosRegistro[i].value;
			}
		}
}
//funcion busca a una persona en el local storage
function encontrarPersona(evt){
	personasJson = JSON.parse(localStorage.getItem("Personas"));
	for (var i in personasJson) {
		if(personasJson[i].correo == correoRegistro.value){
			alert("El correo ya existe");
			encontroPersona = true;
		}
	}
}

//funcion que valida que los campos de ingreso no esten vacios y que la persona se encuentre en el localstorage
function ingresarUsuario(evt){
	firebase.auth().signInWithEmailAndPassword(correoIngreso.value, contrasenaIngreso.value).then(function(){
		usuarioIngresado = firebase.auth().currentUser;
		secciones[1].className = "ocultar";
		secciones[0].className = "animated fadeIn";
		cambiarMenu();
		botones[0].className = "ocultar";
		barraMenu.className = "navbar navbar-inverse navbar-fixed-top absoluto fondo-menu";
		alert("¡Usuario Ingresado!\n"+usuarioIngresado.displayName);
	}).catch(function(error) {
	  // Handle Errors here.
	  errorCode = error.code;
	  errorMessage = error.message;
	  alert("Error en la autenticación");
	  //
	});
}

//cambia el menu cuando el usuario ingresa o cuando sale
function cambiarMenu(evt){
	if(botones[8].className == "btn btn-link ocultar"){
		botones[1].className = "btn btn-link ocultar";
		botones[8].className = "btn btn-link";
	}else{
		botones[1].className = "btn btn-link";
		botones[8].className = "btn btn-link ocultar";
	}

}


//Despliga u oculta el menu en los dispositivos moviles
function desplegarMenu(evt) {
	if(estadoMenu == false && this.id == "menu_desplegable"){
		menu.className = "mostrar navbar-toggle collapsed animated fadeInDown";
		
		estadoMenu = true;
	}
	else if (estadoMenu == true){
	menu.className = "ocultar navbar-toggle collapsed animated fadeOut";
		estadoMenu = false;
	}	
}

//Llena los cursos que el usuario ingresado ya vio
function llenarCursos(evt){
	for (var i in usuarioIngresado.cursos) {
		if(usuarioIngresado.cursos[i] != null){
			listaCursos[i].className = "list-group-item list-group-item-success";
		}
	}
}

//Funcion que permite la navegacion entre las diferentes secciones
function procesarClick(evt)
{
	palabra = this.id;
	seccion = palabra.split("_")[1];
	if (palabra	== "salir_inicio") {
		salirUsuario();
	}
	if(seccion == "inicio"){
		botones[0].className = "ocultar";
		barraMenu.className = "navbar navbar-inverse navbar-fixed-top absoluto fondo-menu";
	}else{
		botones[0].className = "btn btn-link";
		barraMenu.className = "navbar navbar-inverse navbar-fixed-top relativo fondo-menu";
	}
	referencia = document.getElementById(seccion);
	ocultar();
	referencia.className = "animated fadeIn";
	desplegarMenu();

}

//oculta todas las secciones
function ocultar()
{
	for(var i in secciones)
	{
		secciones[i].className = "ocultar";
	}
}